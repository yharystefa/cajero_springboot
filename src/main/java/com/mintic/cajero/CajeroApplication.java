package com.mintic.cajero;

import com.mintic.cajero.logica.Cuenta;
import com.mintic.cajero.logica.Estudiante;
import java.sql.SQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/*import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;*/

@SpringBootApplication // Donde se encuentre este arroba, es el Main.
@RestController // Indica que la clase será un API REST.
public class CajeroApplication {

    @Autowired // Sirve para interpretar una clase sin necesidad de decirle que cree una instancia nueva. Equivalente a c = new Cuenta();
    Cuenta c; // Instancias
    @Autowired
    Estudiante e; // Instancia
    // Ventajas: manejo de excepciones controlado.
    // Evita el SQL Injection que pueden ingresar los hackers porque valida las entradas de los parametros.

    public static void main(String[] args) {
        SpringApplication.run(CajeroApplication.class, args);
    }

// EJEMPLO
    @GetMapping("/hello")
    public String hello(@RequestParam(value = "name", defaultValue = "World") String name,
            @RequestParam(value = "edad", defaultValue = "20") String edad) {
        return String.format("Hello %s, you are %s old!", name, edad);
    }
    

    @GetMapping("/estudiante")
    public String consultarEstudiantePorCedula(@RequestParam(value = "cedula", defaultValue = "1") int cedula) throws ClassNotFoundException, SQLException {
        e.setId(cedula);
        if (e.consultar()) {
            // return String.format(e.toString());
            return "{\"id\":\""+e.getId()+"\",\"nombre\":\""+e.getNombre()+"\"}";
        } else {
            return "{\"value\":\""+"Estudiante no encontrado."+"\"}";
        }
    }

    @GetMapping("/cuenta")
    public String consultarCuentaPorEstudiante(@RequestParam(value = "cedula", defaultValue = "1") int cedula) throws ClassNotFoundException, SQLException {
        c.setIdEstudiante(cedula);
        if (c.consultar()) {
            return "{\"id\":\""+c.getId()+"\",\"numcuenta\":\""+c.getNumero_cuenta()+"\",\"saldo\":\""+c.getSaldo()+"\",\"cvv\":\""+c.getCvv()+"\"}";
        } else {
            return "{\"id\":\""+cedula+"\",\"numcuenta\":\""+"La cédula no tiene asociada una cuenta."+"\"}";
        }
    }
}
