/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

// CLASE HIJA
package com.mintic.cajero.logica;

import java.sql.SQLException;

/**
 *
 * @author frey
 */
public class Retiro extends Transaccion {

    @Override
    public void registrarTransaccion() throws ClassNotFoundException, SQLException {
        this.setTipo("Retiro");
        super.registrarTransaccion();
    }
    
}
