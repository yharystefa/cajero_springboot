/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
angular.module('hello', []) // el $scope continee lo que está en la {{}}
  .controller('home', function($scope, $http) { // Controla las variables que tiene el componente home escrita en angular
    /* let promesa = $http.get('/estudiante');
    promesa.success(function(data){
        $scope.greeting = data;
    });*/
    $http.get('/estudiante?cedula=123').success(function(data) { // Consulta el API y guarda el resultado en data
        // $http - Hace peticiones al servidor GET POST PUT DELETE 
        //$http.get('/estudiante/'). devuelven una intancia de promesa
        console.log(data);
        $scope.greeting = data; // En $scope está la instancia de home
    })
});

//  Pasa algo nos lo indica el - function($scope, $http) 