// JavaScript source code
angular.module('cajero',[])
.controller('filasCuentas',function($scope,$http){
    $scope.consultar = function(){
        if($scope.cedula == undefined || $scope.cedula == null){
            $scope.cedula = 0;
        }
        $http.get("/cuenta?cedula="+$scope.cedula).success(function(data){
            $scope.filas = [
                //{ id: 1, numcuenta: 1351651},
                data
            ];
        });
    };
});